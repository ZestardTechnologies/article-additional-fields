<ul>
    <li>
        You will have to add this shortcode for each of the fields added, as each field have its different shortcode.
        <div class="form-group">
            <div class="showCodeWrapperarea">
                <textarea id="script_code" rows="1" class="form-control script_code" data-app-type="banner-slider" readonly=""><?php echo $field_shortcode; ?></textarea>
            </div>
        </div>
    </li>

    <br/>
    <div class ="row">
        <div class ="col-sm-6">
            <ul>
                <li>Don't change class name for every short-code</li>
                <li>Your id will be your short-code</li>
            </ul>
        </div>
        <div class ="col-sm-6">
            <div class ="screenshot_box">
                <a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/help/help_005.png') }}" target = "_blank">
                    <img class="img-responsive" src="{{ asset('image/help/help_005.png') }}">
                </a>
            </div>
        </div>
    </div>
</ul>