@extends('header')
@section('content')

<div class="basic-container Orders">
    <div class="tab-content">
        <div id="add_order" class="formcolor tab-pane fade in active">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
            <div id="wrap">
                <h1 style="font-size: 20px;color: #697882;font-weight: 400;padding-left: 15px;">Edit Order</h1>
                <div class="add_order_cls" style="background:#ebeef0">
                    <form action="{{ url('update_order/'.$order_id) }}" name="saveform" class="custom-form-design" style="border: 1px solid #ccc;padding: 20px;" onsubmit="return validatemultiform(this);" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group" style="margin-top: -59px; float:right;">
                            <a href="{{ url('orders') }}" value="Back" class="btn btn-primary">Back</a>
                            <input type="submit" name="submit" value="Save and Continue" id="BtnEditOrder" class="btn btn-primary submitform">
                        </div>
                        <div class="panel-body" style="background-color:#fff;">
                            <fieldset>
                                <div class="table-responsive option-box info" style="background-color:#fff;">
                                    <table id="contact_field" class="option-header table info" cellpadding="0" cellspacing="0">
                                        <thead>
                                            <tr style="background:#ebeef0">
                                                <th class="opt-title">Order_ID</th>
                                                <th class="opt-type">User_Id</th>
                                                <th class="opt-req">Total_price</th>
                                                <th class="opt-req">Total_tax</th>
                                                <th class="opt-req">Order_Number</th>
                                                <th class="opt-req"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr style="background:#ebeef0">
                                                <td><?php if (isset($order_id) && $order_id != '') {   echo $order_id; } else { echo ''; } ?></td>
                                <td><?php if (isset($userid) && $userid != '') { echo $userid; } else { echo ''; } ?></td>
                                <td><?php if (isset($totalprice) && $totalprice != '') { echo $totalprice; } else { echo ''; } ?></td>
                                <td><?php if (isset($totaltax) && $totaltax != '') { echo $totaltax; } else { echo ''; } ?></td>
                                <td><?php if (isset($ordernumber) && $ordernumber != '') { echo $ordernumber; } else { echo ''; } ?></td>
                                <td>
                                    <?php if (isset($image) && $image != '') { ?>
                                        <img src="<?php echo $image; ?>" width="100px" height="100px" />
                                    <?php } ?>
                                </td>
                                                
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </fieldset>
                            <?php
                            $data = array();
                            $count = 0;
                            $postdata = array();
                            $output = array();
                            if (isset($store_id) && $store_id != '') {
                                $config = $data['config'] = DB::table('zestard_global_config')->where('store_id', $store_id)->get();                                
                            }
                            ?>
                            <?php //echo "<pre>";print_r($fields); die; ?>
                            <?php if (isset($fields) && !empty($fields)) { ?>                                
                                <?php foreach ($fields AS $field) { ?>
                                    <?php if (isset($order_id)) { ?>
                                        <input type="hidden" name="custom[<?php echo $field->field_id; ?>][OrderId]" value="<?php echo $order_id; ?>" />
                                        <input type="hidden" name="custom[<?php echo $field->field_id; ?>][UserId]" value="<?php echo ($userid != '') ? $userid : ''; ?>" />
                                    <?php } ?>
        <?php $data['row'] = $field; ?> 
        <?php $qry = DB::table('order_customize_table')->where([['field_id', '=', $field->field_id], ['order_id', '=', "" . $order_id . ""]])->get(); ?>                                 
        <?php $data['values'] = (count($qry) > 0) ? $qry[0]->values : ''; ?>    
        <?php $postdata['order_id'] = $order_id; ?>    
                                    <div style="background-color:#ebeef0;">
                                        <div class="panel-body fieldsplayground">
                                            <div class="form-group">                                      
                                                @include('tab_pages/type/'.$field->type,$data)
                                            </div>
                                        </div>
                                    </div>
        <?php $count++; ?>    
    <?php } ?>
<?php } ?>
                            <div class="zestard-multicustomproductform" id=""></div>
                            <div class="form-group" style="margin-top: 20px;float:right;">
                                <a href="{{ url('orders') }}" value="Back" class="btn btn-primary">Back</a>
                                <input type="submit" name="submit" value="Save and Continue" id="BtnEditOrder" class="btn btn-primary submitform">
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div> 
<script type="text/javascript">
    
    function startloader(process) {
        if (process == 1) {
            $(".overlay").css({
                'display': 'block',
                'background-image': 'url({{ asset("image/loader.gif") }})',
                'background-repeat': 'no-repeat',
                'background-attachment': 'fixed',
                'background-position': 'center'
            });
        } else {
            $(".overlay").css({
                'display': 'none',
                'background-image': 'none',
            });
        }
    }

    function validatemultiform(data) {
        var required = 0;
        $('.required-entry').css('border-color','#ccc');
        $('.validation-advice').remove();
        $('.required-entry').each(function() {
            if($.trim($(this).val()) == '' || $(this).val() == null ||  $(this).attr("checked") == undefined) {
                if($(this).hasClass('selection')) {
                    if(!$(this).children().find('.option').is(':checked')){
                        $(this).css('border-color','#df280a');
                        $(this).closest('.form-group').children('.col-sm-6').append('<div class="validation-advice">This is a required field.</div>');
                        required += 1;
                    }
                } else if ($.trim($(this).val()) == '' || $(this).val() == null) {
                    $(this).css('border-color','#df280a');
                    /*if($(this).context.type == 'select-one'){
                        $(this).next('.validation-advice').remove();
                    }*/
                    $(this).closest('.form-group').children('.field').append('<div class="validation-advice">This is a required field.</div>');
                    required += 1;
                }
            }
        });
        if(required) {
            return false;
        }
        startloader(1);
        return true;
    }

</script>
@endsection