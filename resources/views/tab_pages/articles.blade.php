@extends('header')
@section('content')
<div class="basic-container Article">
    <ul class="nav nav-tabs" id="ArticleTab">
        <li class="active"><a data-toggle="tab" href="#article_settings">Article Settings</a></li>
        <li><a data-toggle="tab" href="#article_lists">Article List</a></li>                        
    </ul>
    <div class="tab-content">
        <div id="article_settings" class="formcolor tab-pane fade in active">
            <div class="success-copied"></div>
            <div id="wrap">
                <h1 style="font-size: 20px;color: #697882;font-weight: 400;padding-left: 15px;">
                    Add Form Fields</h1>
                <div class="article_settings_cls" style="background:#ebeef0">
                    <form action="{{ url('article_settings_saveform') }}" name="saveform" class="custom-form-design" style="border: 1px solid #ccc;padding: 20px;" onsubmit="return validatemultiform(this);" method="POST" accept-charset="utf-8">
                        {!! csrf_field() !!}
                        <div class="form-group" style="margin-top: -59px; float:right;">
                            <button type="button" value="" class="btn btn-primary add_field"><span class="glyphicon glyphicon-plus"></span> &nbsp; Add Field</button>
                            <input type="submit" name="submit" value="Save Changes" id="submit" class="btn btn-primary submitform">
                        </div>
                        <div class="panel-body fieldsplayground" style="background-color:#fff;">
                            @include('tab_pages/optionbox')
                            <input type="hidden" id="deletefieldsids" name="contact[deletefieldsids]" value="">
                        </div>
                        <div style="margin-top: 20px;">
                            <input type="submit" name="submit" value="Save Changes" id="submit" class="btn btn-primary submitform">
                        </div>
                    </form>  
                </div>
            </div>

        </div>    
        <div id="article_lists" class="formcolor tab-pane fade">
            <table id="article_list_table" class="table table-striped table-bordered" cellspacing="0" width="100%">                
                <thead>                    
                    <tr>                        
                        <th>#</th>
                        <th>Article Image</th>
                        <th>Article Title</th>
                        <th>Action</th>
                    </tr>

                </thead>                                        
            </table>
        </div>
    </div>
</div>

<script type="text/javascript">
    
    function startloader(process) {
        if (process == 1) {
            $(".overlay").css({
                'display': 'block',
                'background-image': 'url({{ asset("image/loader.gif") }})',
                'background-repeat': 'no-repeat',
                'background-attachment': 'fixed',
                'background-position': 'center'
            });
        } else {
            $(".overlay").css({
                'display': 'none',
                'background-image': 'none',
            });
        }
    }

    $(document).ready(function () {

        var table = $('#article_list_table').DataTable({
            "pageLength": 10,
            "lengthChange": false,
            "paging": true,
            "ordering": true,
            "processing": true,
            "serverSide": true,
            "ajax": '{{url('get_articles')}}',
            "columns": [
                {
                    "data": "0", "orderable": false
                },                
                {
                    "render": function (data, type, JsonResultRow, meta) {
                        return '<img src="' + data + '" style="width:50px;height:50px;">';
                    }
                },
                {
                    "data": "2", "orderable": false
                },
                {
                    "render": function (data, type, JsonResultRow, meta) {
                        return '<a onclick="startloader(1)" href="' + data + '"><span class="glyphicon glyphicon-edit"></span></a>';
                    }
                },
                ],
            "order": [[1, 'asc']]
        });

    });

    function validatemultiform(data) {
        var required = 0;
        $('.required-entry').css('border-color', '#ccc');
        $('.validation-advice').remove();
        $('.required-entry').each(function () {
            if ($.trim($(this).val()) == '' || $(this).val() == null || $(this).attr("checked") == undefined) {
                if ($.trim($(this).val()) == '' || $(this).val() == null) {
                    $(this).css('border-color', '#df280a');
                    /*if ($(this).context.type == 'select-one') {
                        $(this).next('.validation-advice').remove();
                    }*/
                    $(this).closest('table > tbody > tr > td').append('<div class="validation-advice">This is a required field.</div>');
                    required += 1;
                }


            }
        });

        if (required) {
            return false;
        }
        startloader(1);
        return true;
    }
</script>
@endsection



