<label  class="control-label col-sm-4" for="<?php echo $row->type.$row->field_id ?>" <?php echo ($row->is_require) ? 'class="required">'.$row->title.'<em class="text-danger">*</em>': '>'.$row->title; ?></label>
<div class="col-sm-offset-2 col-sm-6 field">
<?php
$class = $row->type.$row->field_id;

$date_field_order = (isset($config[0]->date_field_order) && $config[0]->date_field_order != '') ? $config[0]->date_field_order : 'a:3:{i:0;s:1:"d";i:1;s:1:"m";i:2;s:1:"y";}';
$time_format = (isset($config[0]->time_format) && $config[0]->time_format != '') ? $config[0]->time_format : '1';
$format = unserialize($date_field_order);
$DateFormat = '';
$cnt = 0;
$CollectView = array();
foreach($format as $op) {
	$DateFormat .= ($cnt) ? '/' : '';
	if($op != 'y') {
		$DateFormat .= $CollectView[] = $op.$op;
	} else {
		$DateFormat .= $CollectView[] =$op.$op.$op.$op;
	}
	$cnt++;
}
?>
<div class="input-group date <?php echo $class; ?> col-md-12" data-date="" data-date-format="<?php echo $DateFormat.' HH:ii P'; ?>" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd HH:ii P">
	<input class="<?php echo ($row->is_require) ? 'required-entry':''; ?> form-control" size="16" type="text" name="<?php echo 'custom['.$row->field_id .'][values]'; ?>" id="<?php echo $row->type.$row->field_id ?>" title="<?php echo $row->title ?>" value="<?php echo $values; ?>">
	<span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
	<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
</div>
<script type="text/javascript">
<?php if($time_format == '1') { ?>
 $('.<?php echo $class; ?>').datetimepicker({
	weekStart: 1,
	todayBtn:  1,
	autoclose: 1,
	todayHighlight: 1,
	format: '<?php echo $DateFormat.' HH:ii P'; ?>',
	startView: 2,
	showMeridian: true,
	minView: 0,
	forceParse: 0
});
<?php } else { ?>
$('.<?php echo $class; ?>').datetimepicker({
        language:  'us',
	weekStart: 1,
	todayBtn:  1,
	autoclose: 1,
	todayHighlight: 1,
	startView: 2,
	minView: 0,
	forceParse: 0
});
<?php } ?>
</script>
<span>&nbsp;&nbsp;&nbsp;<?php echo $DateFormat; ?></span>
</div>
