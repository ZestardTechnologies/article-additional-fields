<label class="control-label col-sm-4" for="<?php echo $row->type . $row->field_id ?>"
        <?php echo ($row->is_require) ? 'class="required">' . $row->title . '<em> *</em>' : '>' . $row->title; ?>
</label>
<div class="col-sm-offset-2 col-sm-6 field">
    <input name="<?php echo 'custom['.$row->field_id .'][values]'; ?>" value="<?php echo $values; ?>" class="<?php echo ($row->is_require) ? 'required-entry':''; ?> form-control" id="<?php echo $row->type . $row->field_id ?>" title="<?php echo $row->title ?>" type="<?php echo ($row->validation != 'no') ? 'email' : 'text'; ?>" <?php echo ($row->max_characters != '') ? 'maxlength="' . $row->max_characters . '"' : ''; ?> >

</div>