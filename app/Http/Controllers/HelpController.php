<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use DB;

class HelpController extends Controller {
    
    public function index()
    {
        $app_type = "Articles";
        $app_type_singular = "Article";
        $dashboard_route = "/shopifyapp/article-additional-fields/public/articles";
        $liquid_file_path = "/admin/themes/current/?key=templates/article.liquid";
        $main_shortcode = '<div id="metafields" class="additional_css">&#13;&#10;<h2 class="additional_title"></h2>&#13;&#10;<div class="multi_custom_articals" id="{{article.id}}"></div> </div></div>&#13;&#10;<div class="custom_articals_fields" id="Your short-code"><span>[label]:</span><span>[value]</span></div> &#13;&#10;</div>';
        $field_shortcode = '<div class="custom_articals_fields" id="Your short-code"><span>[label]:</span><span>[value]</span></div>';
        return view('help.help', compact('dashboard_route' , 'app_type' ,'app_type_singular','liquid_file_path','main_shortcode','field_shortcode'));
    }
    
    public function appConfiguration()
    {
        $app_type = "Articles";
        $app_type_singular = "Article";
        $dashboard_route = "/shopifyapp/article-additional-fields/public/articles";
        $liquid_file_path = "/admin/themes/current/?key=templates/article.liquid";
        $main_shortcode = '<div id="metafields" class="additional_css">&#13;&#10;<h2 class="additional_title"></h2>&#13;&#10;<div class="multi_custom_articals" id="{{article.id}}"></div> </div></div>&#13;&#10;<div class="custom_articals_fields" id="Your short-code"><span>[label]:</span><span>[value]</span></div> &#13;&#10;</div>';
        $field_shortcode = '<div class="custom_articals_fields" id="Your short-code"><span>[label]:</span><span>[value]</span></div>';
        return view('app_configuration',compact('dashboard_route' , 'app_type' ,'app_type_singular','liquid_file_path','main_shortcode','field_shortcode'));
    }
}