
var base_path_metafields = "https://zestardshop.com/shopifyapp/article-additional-fields/";
var store_name = Shopify.shop;
var ztpl_metafields = jQuery;

loadCSS = function (href) {
    var cssLink = ztpl_metafields("<link rel='stylesheet' type='text/css' href='" + href + "'>");
    ztpl_metafields("head").append(cssLink);
};
loadJS = function (src) {
    var jsLink = ztpl_metafields("<script src='" + src + "'></script>");
    ztpl_metafields("head").append(jsLink);
};



ztpl_metafields(document).ready(function () {
    // var current = 0;
    loadCSS(base_path_metafields + "public/css/zestard_metafields.css");

    /*---------------------------------------------------Call ajax for collection page view front side data----------------------------*/
    var CollectionDiv = ztpl_metafields('.multi_custom_collections').length;
    var current = 0; 
    if (CollectionDiv > 0)
    {
        //ztpl_metafields('body').append("<div id='loadingbackground' style='display:block; background: #000;position: fixed;height: 100%;width: 100%;top: 0;left: 0;z-index: 1000000;opacity: 0.9;'></div>");
        //ztpl_metafields('body').append('<div id="loadingDiv" style="display: block;height: 200px;width: 200px;position: fixed;z-index: 1000000000;top: 45%;left: 45%;background: url(https://shopifydev.anujdalal.com/dev_metafields/public/image/fieldsloader.gif);"></div>');
        ztpl_metafields('.custom_collection_fields').each(function () {
            var collectionid = ztpl_metafields('.multi_custom_collections').attr('id');
            if (collectionid != '') {
                //ztpl_metafields("#loadingbackground").show();
                //ztpl_metafields("#loadingDiv").show();
            }
            var id = ztpl_metafields(this).attr('id');
            var that = ztpl_metafields(this);
            
            jQuery.ajax({
                url: base_path_metafields + 'public/get_single_collectiondata?shortcode_title=' + id + '&collectionid=' + collectionid + '&shop=' + store_name,
                dataType: "json",
                method: "GET",
                async : true,
                data: {shop:store_name},
                success: function (data) {
                   // console.log(data);
                    if (data == 'err') {
                        that.html(that.html().replace("<b>[label]</b> : ", ''));
                        that.html(that.html().replace("[label] : ", ''));
                        that.html(that.html().replace("[value]", ''));
                        ztpl_metafields(".multi_custom_collections").html('<div class="alert alert-danger"> <strong>Oops! Something went wrong. </strong></div>');
                    }
                    if (data == 'error') {
                        that.html(that.html().replace("<b>[label]</b> : ", ''));
                        that.html(that.html().replace("[value]", ''));
                    } else {
                       
                        ztpl_metafields.each(data, function (key, value1) {
                            //console.log(value1.additional_title);
                            //ztpl_metafields('.additional_css').after("<style>" + value1.additional_css + "</style>");
                            if (value1.values != '') {
                                that.html(that.html().replace("[label]", value1.title));
                                that.html(that.html().replace("[label] : ", ''));
                                that.html(that.html().replace("[value]", value1.values));
                                if(current==1) { ztpl_metafields('.additional_css').after("<style>" + value1.additional_css + "</style>"); }
                                ztpl_metafields('.additional_title').text(value1.additional_title);
                            } else {
                                that.html(that.html().replace("<b>[label]</b> : ", ''));
                                that.html(that.html().replace("[label] : ", ''));
                                that.html(that.html().replace("[value]", ''));
                            }
                          current++;
                          //console.log(current);
                        });
                    }
                    //ztpl_metafields("#loadingbackground").hide();
                    //ztpl_metafields("#loadingDiv").hide();
                },
                error: function (xhr, status, err) {
                    jQuery("<p>Error: Status = " + status + ", err = " + err + "</p>")
                            .appendTo(document.body);
                    //ztpl_metafields("#loadingbackground").hide();
                    //ztpl_metafields("#loadingDiv").hide();
                }
            });


        });
    }

    /*---------------------------------------------------Call ajax for page view front side data----------------------------*/
    var PageDiv = ztpl_metafields('.multi_custom_pages').length;
    var current = 0;
    if (PageDiv > 0)
    {
        //ztpl_metafields('body').append("<div id='loadingbackground' style='display:block; background: #000;position: fixed;height: 100%;width: 100%;top: 0;left: 0;z-index: 1000000;opacity: 0.9;'></div>");
        //ztpl_metafields('body').append('<div id="loadingDiv" style="display: block;height: 200px;width: 200px;position: fixed;z-index: 1000000000;top: 45%;left: 45%;background: url(https://shopifydev.anujdalal.com/dev_metafields/public/image/fieldsloader.gif);"></div>');
        ztpl_metafields('.custom_page_fields').each(function () {
            var pageid = ztpl_metafields('.multi_custom_pages').attr('id');
            if (pageid != '') {
                //ztpl_metafields("#loadingbackground").show();
                //ztpl_metafields("#loadingDiv").show();
            }
            var id = ztpl_metafields(this).attr('id');
            var that = ztpl_metafields(this);
            jQuery.ajax({
                url: base_path_metafields + 'public/get_single_pagedata?shortcode_title=' + id + '&pageid=' + pageid + '&shop=' + store_name,
                dataType: "json",
                method: "get",
                data: {shop:store_name},
                success: function (data) {
                    //console.log(data);
                    if (data == 'err') {
                        that.html(that.html().replace("<b>[label]</b> : ", ''));
                        that.html(that.html().replace("[label] : ", ''));
                        that.html(that.html().replace("[value]", ''));
                        ztpl_metafields(".multi_custom_pages").html('<div class="alert alert-danger"> <strong>Oops! Something went wrong. </strong></div>');
                    }
                    if (data == 'error') {
                        that.html(that.html().replace("<b>[label]</b> : ", ''));
                        that.html(that.html().replace("[label] : ", ''));
                        that.html(that.html().replace("[value]", ''));
                    } else {
                        ztpl_metafields.each(data, function (key, value1) {
                            if (value1.values != '') {
                                that.html(that.html().replace("[label]", value1.title));
                                that.html(that.html().replace("[value]", value1.values));
                                if(current==1) { ztpl_metafields('.additional_css').after("<style>" + value1.additional_css + "</style>"); }
                                ztpl_metafields('.additional_title').text(value1.additional_title);
                            } else {
                                that.html(that.html().replace("<b>[label]</b> : ", ''));
                                that.html(that.html().replace("[label] : ", ''));
                                that.html(that.html().replace("[value]", ''));
                            }
                            current++;
                        });
                    }
                    //ztpl_metafields("#loadingbackground").hide();
                    //ztpl_metafields("#loadingDiv").hide();
                },
                error: function (xhr, status, err) {
                    jQuery("<p>Error: Status = " + status + ", err = " + err + "</p>")
                            .appendTo(document.body);
                    //ztpl_metafields("#loadingbackground").hide();
                    //ztpl_metafields("#loadingDiv").hide();
                }
            });
        });


    }

    /*---------------------------------------------------Call ajax for blog page view front side data----------------------------*/
    var BlogDiv = ztpl_metafields('.multi_custom_blogs').length;
    var current = 0;
    if (BlogDiv > 0)
    {
        //ztpl_metafields('body').append("<div id='loadingbackground' style='display:block; background: #000;position: fixed;height: 100%;width: 100%;top: 0;left: 0;z-index: 1000000;opacity: 0.9;'></div>");
        //ztpl_metafields('body').append('<div id="loadingDiv" style="display: block;height: 200px;width: 200px;position: fixed;z-index: 1000000000;top: 45%;left: 45%;background: url(https://shopifydev.anujdalal.com/dev_metafields/public/image/fieldsloader.gif);"></div>');
        ztpl_metafields('.custom_blog_fields').each(function () {
            var blogid = ztpl_metafields('.multi_custom_blogs').attr('id');
            if (blogid != '') {
                //ztpl_metafields("#loadingbackground").show();
                //ztpl_metafields("#loadingDiv").show();
            }
            var id = ztpl_metafields(this).attr('id');
            var that = ztpl_metafields(this);
            jQuery.ajax({
                url: base_path_metafields + 'public/get_single_blogdata?shortcode_title=' + id + '&blogid=' + blogid + '&shop=' + store_name,
                dataType: "json",
                method: "get",
                data: {shop:store_name},
                success: function (data) {
                    //console.log(data);
                    if (data == 'err') {
                        that.html(that.html().replace("<b>[label]</b> : ", ''));
                        that.html(that.html().replace("[label] : ", ''));
                        that.html(that.html().replace("[value]", ''));
                        ztpl_metafields(".multi_custom_blogs").html('<div class="alert alert-danger"> <strong>Oops! Something went wrong. </strong></div>');
                    }
                    if (data == 'error') {
                        that.html(that.html().replace("<b>[label]</b> : ", ''));
                        that.html(that.html().replace("[label] : ", ''));
                        that.html(that.html().replace("[value]", ''));
                    } else {
                        ztpl_metafields.each(data, function (key, value1) {
                            if (value1.values != '') {
                                that.html(that.html().replace("[label]", value1.title));
                                that.html(that.html().replace("[value]", value1.values));
                                if(current==1) { ztpl_metafields('.additional_css').after("<style>" + value1.additional_css + "</style>"); }
                                ztpl_metafields('.additional_title').text(value1.additional_title);
                            } else {
                                that.html(that.html().replace("<b>[label]</b> : ", ''));
                                that.html(that.html().replace("[label] : ", ''));
                                that.html(that.html().replace("[value]", ''));
                            }
                            current++;
                        });
                    }
                    //ztpl_metafields("#loadingbackground").hide();
                    //ztpl_metafields("#loadingDiv").hide();
                },
                error: function (xhr, status, err) {
                    jQuery("<p>Error: Status = " + status + ", err = " + err + "</p>")
                            .appendTo(document.body);
                    //ztpl_metafields("#loadingbackground").hide();
                    //ztpl_metafields("#loadingDiv").hide();
                }
            });
        });


    }

    /*---------------------------------------------------Call ajax for artical page view front side data----------------------------*/
    var ArticleDiv = ztpl_metafields('.multi_custom_articals').length;
    var current = 0;
    if (ArticleDiv > 0)
    {
        //ztpl_metafields('body').append("<div id='loadingbackground' style='display:block; background: #000;position: fixed;height: 100%;width: 100%;top: 0;left: 0;z-index: 1000000;opacity: 0.9;'></div>");
        //ztpl_metafields('body').append('<div id="loadingDiv" style="display: block;height: 200px;width: 200px;position: fixed;z-index: 1000000000;top: 45%;left: 45%;background: url(https://shopifydev.anujdalal.com/dev_metafields/public/image/fieldsloader.gif);"></div>');
        ztpl_metafields('.custom_articals_fields').each(function () {
            var articalid = ztpl_metafields('.multi_custom_articals').attr('id');
            if (articalid != '') {
                //ztpl_metafields("#loadingbackground").show();
                //ztpl_metafields("#loadingDiv").show();
            }
            var id = ztpl_metafields(this).attr('id');
            var that = ztpl_metafields(this);
            jQuery.ajax({
                url: base_path_metafields + 'public/get_single_articledata?shortcode_title=' + id + '&articalid=' + articalid + '&shop=' + store_name,
                dataType: "json",
                method: "get",
                data: {shop:store_name},
                success: function (data) {
                    //console.log(data);
                    if (data == 'err') {
                        that.html(that.html().replace("<b>[label]</b> : ", ''));
                        that.html(that.html().replace("[label] : ", ''));
                        that.html(that.html().replace("[value]", ''));
                        ztpl_metafields(".multi_custom_articals").html('<div class="alert alert-danger"> <strong>Oops! Something went wrong. </strong></div>');
                    }
                    if (data == 'error') {
                        that.html(that.html().replace("<b>[label]</b> : ", ''));
                        that.html(that.html().replace("[label] : ", ''));
                        that.html(that.html().replace("[value]", ''));
                    } else {
                        ztpl_metafields.each(data, function (key, value1) {
                            if (value1.values != '') {
                                that.html(that.html().replace("[label]", value1.title));
                                that.html(that.html().replace("[value]", value1.values));
                                if(current==1) { ztpl_metafields('.additional_css').after("<style>" + value1.additional_css + "</style>"); }
                                ztpl_metafields('.additional_title').text(value1.additional_title);
                            } else {
                                that.html(that.html().replace("<b>[label]</b> : ", ''));
                                that.html(that.html().replace("[label] : ", ''));
                                that.html(that.html().replace("[value]", ''));
                            }
                            current++;
                        });
                    }
                    //ztpl_metafields("#loadingbackground").hide();
                    //ztpl_metafields("#loadingDiv").hide();
                },
                error: function (xhr, status, err) {
                    jQuery("<p>Error: Status = " + status + ", err = " + err + "</p>")
                            .appendTo(document.body);
                    //ztpl_metafields("#loadingbackground").hide();
                    //ztpl_metafields("#loadingDiv").hide();
                }
            });
        });

    }

    /*---------------------------------------------------Call ajax for order page view front side data----------------------------*/
    var OrderDiv = ztpl_metafields('.multi_custom_orders').length;
    var current = 0;
    if (OrderDiv > 0)
    {
        //ztpl_metafields('body').append("<div id='loadingbackground' style='display:block; background: #000;position: fixed;height: 100%;width: 100%;top: 0;left: 0;z-index: 1000000;opacity: 0.9;'></div>");
        //ztpl_metafields('body').append('<div id="loadingDiv" style="display: block;height: 200px;width: 200px;position: fixed;z-index: 1000000000;top: 45%;left: 45%;background: url(https://shopifydev.anujdalal.com/dev_metafields/public/image/fieldsloader.gif);"></div>');
        ztpl_metafields('.custom_orders_fields').each(function () {
            var orderid = ztpl_metafields('.multi_custom_orders').attr('id');
            if (orderid != '') {
                //ztpl_metafields("#loadingbackground").show();
                //ztpl_metafields("#loadingDiv").show();
            }
            var id = ztpl_metafields(this).attr('id');
            var that = ztpl_metafields(this);
            jQuery.ajax({
                url: base_path_metafields + 'public/get_single_orderdata?shortcode_title=' + id + '&orderid=' + orderid + '&shop=' + store_name,
                dataType: "json",
                method: "get",
                data: {shop:store_name},
                success: function (data) {
                    //console.log(data);
                    if (data == 'err') {
                        that.html(that.html().replace("<b>[label]</b> : ", ''));
                        that.html(that.html().replace("[label] : ", ''));
                        that.html(that.html().replace("[value]", ''));
                        ztpl_metafields(".multi_custom_orders").html('<div class="alert alert-danger"> <strong>Oops! Something went wrong. </strong></div>');
                    }
                    if (data == 'error') {
                        that.html(that.html().replace("<b>[label]</b> : ", ''));
                        that.html(that.html().replace("[label] : ", ''));
                        that.html(that.html().replace("[value]", ''));
                    } else {
                        ztpl_metafields.each(data, function (key, value1) {
                            if (value1.values != '') {
                                that.html(that.html().replace("[label]", value1.title));
                                that.html(that.html().replace("[value]", value1.values));
                                if(current==1) { ztpl_metafields('.additional_css').after("<style>" + value1.additional_css + "</style>"); }
                                ztpl_metafields('.additional_title').text(value1.additional_title);
                            } else {
                                that.html(that.html().replace("<b>[label]</b> : ", ''));
                                that.html(that.html().replace("[label] : ", ''));
                                that.html(that.html().replace("[value]", ''));
                            }
                            current++;
                        });
                    }
                    //ztpl_metafields("#loadingbackground").hide();
                    //ztpl_metafields("#loadingDiv").hide();
                },
                error: function (xhr, status, err) {
                    jQuery("<p>Error: Status = " + status + ", err = " + err + "</p>")
                            .appendTo(document.body);
                    //ztpl_metafields("#loadingbackground").hide();
                    //ztpl_metafields("#loadingDiv").hide();
                }
            });
        });

    }

    /*---------------------------------------------------Call ajax for Customer page view front side data----------------------------*/
    var CustomerDiv = ztpl_metafields('.multi_custom_customers').length;
    var current = 0;
    if (CustomerDiv > 0)
    {

        //ztpl_metafields('body').append("<div id='loadingbackground' style='display:block; background: #000;position: fixed;height: 100%;width: 100%;top: 0;left: 0;z-index: 1000000;opacity: 0.9;'></div>");
        //ztpl_metafields('body').append('<div id="loadingDiv" style="display: block;height: 200px;width: 200px;position: fixed;z-index: 1000000000;top: 45%;left: 45%;background: url(https://shopifydev.anujdalal.com/dev_metafields/public/image/fieldsloader.gif);"></div>');
        ztpl_metafields('.custom_customers_fields').each(function () {
            var customerid = ztpl_metafields('.multi_custom_customers').attr('id');
            if (customerid != '') {
                //ztpl_metafields("#loadingbackground").show();
                //ztpl_metafields("#loadingDiv").show();
            }
            var id = ztpl_metafields(this).attr('id');
            var that = ztpl_metafields(this);
            jQuery.ajax({
                url: base_path_metafields + 'public/get_single_customerdata?shortcode_title=' + id + '&customerid=' + customerid + '&shop=' + store_name,
                dataType: "json",
                method: "get",
                data: {shop:store_name},
                success: function (data) {
                    //console.log(data);
                    if (data == 'err') {
                        that.html(that.html().replace("<b>[label]</b> : ", ''));
                        that.html(that.html().replace("[label] : ", ''));
                        that.html(that.html().replace("[value]", ''));
                        ztpl_metafields(".multi_custom_customers").html('<div class="alert alert-danger"> <strong>Oops! Something went wrong. </strong></div>');
                    }
                    if (data == 'error') {
                        that.html(that.html().replace("<b>[label]</b> : ", ''));
                        that.html(that.html().replace("[label] : ", ''));
                        that.html(that.html().replace("[value]", ''));
                    } else {
                        ztpl_metafields.each(data, function (key, value1) {
                            if (value1.values != '') {
                                that.html(that.html().replace("[label]", value1.title));
                                that.html(that.html().replace("[value]", value1.values));
                                if(current==1) { ztpl_metafields('.additional_css').after("<style>" + value1.additional_css + "</style>"); }
                                ztpl_metafields('.additional_title').text(value1.additional_title);
                            } else {
                                that.html(that.html().replace("<b>[label]</b> : ", ''));
                                that.html(that.html().replace("[label] : ", ''));
                                that.html(that.html().replace("[value]", ''));
                            }
                            current++;
                        });
                    }
                    //ztpl_metafields("#loadingbackground").hide();
                    //ztpl_metafields("#loadingDiv").hide();
                },
                error: function (xhr, status, err) {
                    jQuery("<p>Error: Status = " + status + ", err = " + err + "</p>")
                            .appendTo(document.body);
                    //ztpl_metafields("#loadingbackground").hide();
                    //ztpl_metafields("#loadingDiv").hide();
                }
            });
        });

    }

    /*---------------------------------------------------Call ajax for Product page view front side data----------------------------*/
    var ProductDiv = ztpl_metafields('.multi_custom_products').length;
    var current = 0;
    if (ProductDiv > 0)
    {

        //ztpl_metafields('body').append("<div id='loadingbackground' style='display:block; background: #000;position: fixed;height: 100%;width: 100%;top: 0;left: 0;z-index: 1000000;opacity: 0.9;'></div>");
        //ztpl_metafields('body').append('<div id="loadingDiv" style="display: block;height: 200px;width: 200px;position: fixed;z-index: 1000000000;top: 45%;left: 45%;background: url(https://shopifydev.anujdalal.com/dev_metafields/public/image/fieldsloader.gif);"></div>');
        ztpl_metafields('.custom_products_fields').each(function () {
            var productid = ztpl_metafields('.multi_custom_products').attr('id');
            if (productid != '') {
                //ztpl_metafields("#loadingbackground").show();
                //ztpl_metafields("#loadingDiv").show();
            }
            var id = ztpl_metafields(this).attr('id');
            var that = ztpl_metafields(this);
            jQuery.ajax({
                url: base_path_metafields + 'public/get_single_productdata?shortcode_title=' + id + '&productid=' + productid + '&shop=' + store_name,
                dataType: "json",
                method: "get",
                data: {shop:store_name},
                success: function (data) {
                    //console.log(data);
                    if (data == 'err') {
                        that.html(that.html().replace("<b>[label]</b> : ", ''));
                        that.html(that.html().replace("[label] : ", ''));
                        that.html(that.html().replace("[value]", ''));
                        ztpl_metafields(".multi_custom_products").html('<div class="alert alert-danger"> <strong>Oops! Something went wrong. </strong></div>');
                    }
                    if (data == 'error') {
                        that.html(that.html().replace("<b>[label]</b> : ", ''));
                        that.html(that.html().replace("[label] : ", ''));
                        that.html(that.html().replace("[value]", ''));
                    } else {
                        ztpl_metafields.each(data, function (key, value1) {
                            if (value1.values != '') {
                                that.html(that.html().replace("[label]", value1.title));
                                that.html(that.html().replace("[value]", value1.values));
                                if(current==1) { ztpl_metafields('.additional_css').after("<style>" + value1.additional_css + "</style>"); }
                                ztpl_metafields('.additional_title').text(value1.additional_title);
                            } else {
                                that.html(that.html().replace("<b>[label]</b> : ", ''));
                                that.html(that.html().replace("[label] : ", ''));
                                that.html(that.html().replace("[value]", ''));
                            }
                            current++;
                        });
                    }
                    //ztpl_metafields("#loadingbackground").hide();
                    //ztpl_metafields("#loadingDiv").hide();
                },
                error: function (xhr, status, err) {
                    jQuery("<p>Error: Status = " + status + ", err = " + err + "</p>")
                            .appendTo(document.body);
                    //ztpl_metafields("#loadingbackground").hide();
                    //ztpl_metafields("#loadingDiv").hide();
                }
            });
        });


    }


});