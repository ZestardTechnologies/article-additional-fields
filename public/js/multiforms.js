$(document).ready(function() {
	var globalFields;
	globalFields = $('.option-box').length;
	var hiddenFields;
	$('.add_field').click(function() {
            	globalFields++;
		var newhtml = formCreator(globalFields);
		//$('.panel-body.fieldsplayground').append(newhtml);
                $('.panel-body.fieldsplayground').prepend(newhtml);                
        });
        $('button.submitform').click(function() {
		$('.form-list:hidden').remove();
		$('tr:hidden').remove();
	});
        function formCreator(globalFields) {
		var htmlformdata;
		htmlformdata = '';
		htmlformdata += '<div class="table-responsive option-box tr_info" id="field_'+globalFields+'">';
		htmlformdata += '<table id="contact_field" class="option-header table tr_info" cellpadding="0" cellspacing="0">';
		htmlformdata += '<thead><tr class="tr_info"><th class="opt-title">Title <span class="required">*</span></th><th class="opt-type">Input Type <span class="required">*</span></th><th class="opt-req">Is Required</th>';
		htmlformdata += '<th class="a-right"><button id="" title="Delete Option" type="button" class="scalable delete btn btn-danger" onclick="deleteField('+globalFields+')" style=""><span><i class="fa fa-close"></i></span></button></th>';
		htmlformdata += '</tr></thead><tbody><tr class="tr_info"><td>';
		htmlformdata += '<input type="text" class="form-control required-entry input-text" id="contact_field_'+globalFields+'_title" name="contact[fields]['+ globalFields +'][title]" ></td><td>';
		htmlformdata += '<select name="contact[fields]['+globalFields+'][type]" id="contact_field_'+ globalFields + '_type" class="form-control required-entry" onchange="choosoption(this,'+globalFields+')" title="" >';
		htmlformdata += '<option value="">-- Please select --</option><optgroup label="Text"><option value="field">Field</option><option value="area">Area</option></optgroup>';
                htmlformdata += '<optgroup label="Video"><option value="video">Video</option></optgroup>';
		htmlformdata += '<optgroup label="File"><option value="file">File</option></optgroup><optgroup label="Select"><option value="drop_down">Drop-down</option><option value="radio">Radio Buttons</option><option value="checkbox">Checkbox</option><option value="multiple">Multiple Select</option></optgroup>';
		htmlformdata += '<optgroup label="Date"><option value="date">Date</option><option value="date_time">Date &amp; Time</option><option value="time">Time</option></optgroup></select></td><td class="opt-req">';
		htmlformdata += '<select name="contact[fields]['+globalFields+'][is_require]" id="contact_field_'+ globalFields +'_is_require" class="form-control" title="">';
		htmlformdata += '<option value="1">Yes</option><option value="0">No</option></select></td>';
		htmlformdata += '</td><td>&nbsp;</td></tr></tbody></table></div>';
		return htmlformdata;
	}
});

function deleteField(fieldval) {
	var fieldid = $('.fieldid'+fieldval).val();
	if(fieldid != undefined){
		var currVal = $("#deletefieldsids").val();
		if(currVal != '') {
			currVal = currVal.split(",");
			currVal.push(fieldid);
			currVal.join(",");
			$("#deletefieldsids").val(currVal);
		} else {
			$("#deletefieldsids").val(fieldid);
		}
	}
	$('#field_'+fieldval).remove();
}
/****************************SUB-FIELDS************************************/
function choosoption(selctedval, fieldval) {
	var option = $(selctedval).find('option:selected').val();
	if(option == 'field'){
            typetext(fieldval);
	} else if (option == 'area') {
            typearea(fieldval);
        }
        else if(option == 'video'){
            typevideo(fieldval);
	}
        else if (option == 'file') {
		typefile(fieldval);
	} else if(option == 'drop_down' || option == 'radio' || option == 'checkbox' || option == 'multiple') {
		typeselect(fieldval);
	} else if(option == 'date_time' || option == 'time' || option == 'date'){
		typedatetime(fieldval);
	}
}

function typetext(fieldval) {
        var typetextorarea;
	typetextorarea = '';
	typetextorarea += '<div id="contact_field_'+fieldval+'_type_text" class="table-responsive  grid tier col-sm-7 form-list">';
	typetextorarea += '<table class="table border" cellpadding="0" cellspacing="0"><tbody><tr class="tr_info headings"><th class="type-validation">Validation</th><th class="type-last last">Max Characters</th></tr><tr class="tr_info"><td>';
	typetextorarea += '<select name="contact[fields]['+fieldval+'][validation]" id="contact_field_'+fieldval+'_validation" class="select" title=""><option value="no">No</option><option value="email">Email</option></select></td><td class="type-last last">';
	typetextorarea += '<input type="text" class="form-control required-entry input-text validate-zero-or-greater" name="contact[fields]['+fieldval+'][max_characters]" value="100"></td></tr></tbody></table></div>';
	$('#contact_field_'+fieldval+'_type_file').hide();
	$('#contact_field_'+fieldval+'_type_select').hide();
        $('#contact_field_'+fieldval+'_type_textarea').hide();
        $('#contact_field_'+fieldval+'_type_video').hide();
	if(!$('#contact_field_'+fieldval+'_type_text').length) {
            $('#field_'+fieldval).append(typetextorarea);
	} else {
            $('#contact_field_'+fieldval+'_type_text').show();
	}
}

function typearea(fieldval) {
        var typetextorarea;
	typetextorarea = '';
	typetextorarea += '<div id="contact_field_'+fieldval+'_type_textarea" class="table-responsive  grid tier col-sm-7 form-list">';
	typetextorarea += '<table class="table border" cellpadding="0" cellspacing="0"><tbody><tr class="tr_info headings"><th class="type-last last">Max Characters</th></tr><tr class="tr_info"><td>';
	//typetextorarea += '<select name="contact[fields]['+fieldval+'][validation]" id="contact_field_'+fieldval+'_validation" class="select" title=""><option value="no">No</option><option value="email">Email</option></select></td><td class="type-last last">';
	typetextorarea += '<input type="text" class="form-control required-entry input-text validate-zero-or-greater" name="contact[fields]['+fieldval+'][max_characters]" value="100"></td></tr></tbody></table></div>';
	$('#contact_field_'+fieldval+'_type_file').hide();
	$('#contact_field_'+fieldval+'_type_select').hide();
        $('#contact_field_'+fieldval+'_type_text').hide();
        $('#contact_field_'+fieldval+'_type_video').hide();
	if(!$('#contact_field_'+fieldval+'_type_textarea').length) {
            $('#field_'+fieldval).append(typetextorarea);
	} else {
            $('#contact_field_'+fieldval+'_type_textarea').show();
        }
}

function typevideo(fieldval) {
        var typevideo;
	typevideo = '';
	typevideo += '<div id="contact_field_'+fieldval+'_type_video" class="table-responsive  grid tier col-sm-7 form-list">';
	typevideo += '<table class="table border" cellpadding="0" cellspacing="0"><tbody><tr class="tr_info headings"><th class="type-last last">Height</th><th class="type-last last">Width</th></tr><tr class="tr_info"><td>';
	typevideo += '<input type="text" class="form-control required-entry input-text validate-zero-or-greater" name="contact[fields]['+fieldval+'][height]" value=""></td>';
        typevideo += '<td><input type="text" class="form-control required-entry input-text validate-zero-or-greater" name="contact[fields]['+fieldval+'][width]" value=""></td></tr></tbody></table></div>';
	$('#contact_field_'+fieldval+'_type_file').hide();
	$('#contact_field_'+fieldval+'_type_select').hide();
        $('#contact_field_'+fieldval+'_type_text').hide();
        $('#contact_field_'+fieldval+'_type_textarea').hide();
	if(!$('#contact_field_'+fieldval+'_type_video').length) {
            $('#field_'+fieldval).append(typevideo);
	} else {
            $('#contact_field_'+fieldval+'_type_video').show();
        }
}

function typefile(fieldval) {
	var typefile;
	typefile = '';
	typefile += '<div id="contact_field_'+fieldval+'_type_file" class="table-responsive col-sm-7 grid tier form-list">';
	typefile += '<table class="table border" cellpadding="0" cellspacing="0"><tbody><tr class="headings tr_info"><th class="type-title">Allowed File Extensions</th><th class="last">Maximum File Size</th><th class="type-last last">Height</th><th class="type-last last">Width</th></tr><tr class="tr_info"><td><input class="form-control required-entry input-text" type="text" name="contact[fields]['+fieldval+'][file_extension]" value="" ><span>(jpg,jpeg,png,gif)</span></td><td class="type-last last" nowrap=""><input class="form-control required-entry input-text" type="text" name="contact[fields]['+fieldval+'][file_size]"  > MB</td><td><input type="text" class="form-control required-entry input-text validate-zero-or-greater" name="contact[fields]['+fieldval+'][height]" value=""></td><td><input type="text" class="form-control required-entry input-text validate-zero-or-greater" name="contact[fields]['+fieldval+'][width]" value=""></td></tr></tbody></table></div>';
	$('#contact_field_'+fieldval+'_type_text').hide();
        $('#contact_field_'+fieldval+'_type_textarea').hide();
	$('#contact_field_'+fieldval+'_type_select').hide();
        $('#contact_field_'+fieldval+'_type_video').hide();
	if(!$('#contact_field_'+fieldval+'_type_file').length) {
		$('#field_'+fieldval).append(typefile);
	} else {
		$('#contact_field_'+fieldval+'_type_file').show();
	}
}

function typeselect(fieldval) {
	var typeselect;
	typeselect = '';
	typeselect += '<div id="contact_field_'+fieldval+'_type_select" class="grid col-sm-7 tier form-list">';
	typeselect += '<table class="table border" cellpadding="0" cellspacing="0">';
	typeselect += '<thead><tr class="tr_info headings"><th class="type-title">Title<span class="">*</span></th>';
	typeselect += '<th class="type-butt last">&nbsp;</th></tr></thead><tbody id="select_field_type_row_'+fieldval+'">';
        
        //Vj-start
        typeselect += '<tr class="subchildrows" id="contact_field_'+fieldval+'_select_'+fieldval+'"><td><input type="text" class="form-control required-entry input-text select-type-title" id="contact_field_'+fieldval+'_select_'+fieldval+'_title" name="contact[fields]['+fieldval+'][values]['+fieldval+'][title]" value=""></td><td class="last"></td></tr>';        //Vj-end
        
        
        typeselect += '</tbody><tfoot><tr class="tr_info">';
	typeselect += '<td colspan="100" class="a-right"><button id="add_select_row_button_'+fieldval+'" title="Add New Row" type="button" class="scalable add add-select-row btn btn-primary" onclick="addrow('+fieldval+')" style="float:right;"><span><span><span class="glyphicon glyphicon-plus"></span></span></span></button></td>';
	typeselect += '</tr></tfoot></table></div>';
	$('#contact_field_'+fieldval+'_type_text').hide();
        $('#contact_field_'+fieldval+'_type_video').hide();
	$('#contact_field_'+fieldval+'_type_file').hide();
        $('#contact_field_'+fieldval+'_type_textarea').hide();
	if(!$('#contact_field_'+fieldval+'_type_select').length) {
		$('#field_'+fieldval).append(typeselect);
	} else {
		$('#contact_field_'+fieldval+'_type_select').show();
	}
}

function addrow(fieldval) {
	var childFields;
	childFields = $('#select_field_type_row_'+fieldval+' > .subchildrows').length;
	childFields++;
	var addrow;
	addrow = '';
	addrow += '<tr class="subchildrows" id="contact_field_'+fieldval+'_select_'+childFields+'">';
	addrow += '<td>';
	addrow += '<input type="text" class="form-control required-entry input-text select-type-title" id="contact_field_'+fieldval+'_select_'+childFields+'_title" name="contact[fields]['+fieldval+'][values]['+childFields+'][title]"  value=""  ></td>';
	addrow += '<td class="last"><span title="Delete row"><button id="id_'+childFields+'" title="Delete Row" type="button" class="scalable delete delete-select-row btn btn-warning icon-btn" onclick="removeRow('+childFields+','+fieldval+')" style=""><span><span><span><i class="glyphicon glyphicon-remove"></i></span></span></span></button></span></td></tr>';
	$('#select_field_type_row_'+fieldval).append(addrow);
	
}

function typedatetime(fieldval) {
	$('#contact_field_'+fieldval+'_type_text').hide();
	$('#contact_field_'+fieldval+'_type_file').hide();
	$('#contact_field_'+fieldval+'_type_select').hide();
        $('#contact_field_'+fieldval+'_type_textarea').hide();
        $('#contact_field_'+fieldval+'_type_video').hide();
}

function removeRow(childFields, fieldval) {
    $('#contact_field_'+fieldval+'_select_'+childFields+'_title').removeClass('required-entry');
    $('#contact_field_'+fieldval+'_select_'+childFields+'_title').attr("disabled", true);
    $('#contact_field_'+fieldval+'_select_'+childFields).hide();
}
/**************************************************************************/
