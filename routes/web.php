<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('dashboard', 'callbackController@dashboard')->name('dashboard');
Route::get('callback', 'callbackController@index')->name('callback');
Route::get('redirect', 'callbackController@redirect')->name('redirect');
Route::get('payment_process', 'callbackController@payment_method')->name('payment_process');
Route::get('payment_success/{shop}', 'callbackController@payment_compelete')->name('payment_success');
Route::get('frontend', 'FrontendController@frontend')->middleware('cors')->name('frontend');
Route::get('declined', 'callbackController@declined')->name('declined');

Route::get('decline', function () {
    return view('decline');
})->name('decline');

Route::get('help', 'HelpController@index')->name('help');

Route::get('global_config', function () {
    return view('global_config');
})->name('global_config');

Route::get('app_configuration', 'HelpController@appConfiguration')->name('app_configuration');

Route::get('globalconfig', 'GlobalConfigController@index')->name('globalconfig');
Route::post('save_form_global_config', 'GlobalConfigController@save_form_global_config')->name('save_form_global_config');

Route::get('collections', 'CollectionController@index')->name('collections');
Route::get('get_custome_collections', 'CollectionController@get_custome_collections')->name('get_custome_collections');
Route::post('collection_settings_saveform', 'CollectionController@collection_settings_saveform')->name('collection_settings_saveform');
Route::get('edit_collection/{id}', 'CollectionController@edit_collection')->name('edit_collection');
Route::post('update_collection/{id}', 'CollectionController@update_collection')->name('update_collection');
Route::get('get_single_collectiondata', 'CollectionController@get_single_collectiondata')->middleware('cors')->name('get_single_collectiondata');


Route::get('pages', 'PageController@index')->name('pages');
Route::get('get_pages', 'PageController@get_pages')->name('get_pages');
Route::post('page_settings_saveform', 'PageController@page_settings_saveform')->name('page_settings_saveform');
Route::get('edit_page/{id}', 'PageController@edit_page')->name('edit_page');
Route::post('update_page/{id}', 'PageController@update_page')->name('update_page');
Route::get('get_single_pagedata', 'PageController@get_single_pagedata')->middleware('cors')->name('get_single_pagedata');

Route::get('blogs', 'BlogController@index')->name('blogs');
Route::get('get_blogs', 'BlogController@get_blogs')->name('get_blogs');
Route::post('blog_settings_saveform', 'BlogController@blog_settings_saveform')->name('blog_settings_saveform');
Route::get('edit_blog/{id}', 'BlogController@edit_blog')->name('edit_blog');
Route::post('update_blog/{id}', 'BlogController@update_blog')->name('update_blog');
Route::get('get_single_blogdata', 'BlogController@get_single_blogdata')->middleware('cors')->name('get_single_blogdata');


Route::get('articles', 'ArticleController@index')->name('articles');
Route::get('get_articles', 'ArticleController@get_articles')->name('get_articles');
Route::post('article_settings_saveform', 'ArticleController@article_settings_saveform')->name('article_settings_saveform');
Route::get('edit_article/{id}/{blog_id}', 'ArticleController@edit_article')->name('edit_article');
Route::post('update_article/{id}/{blog_id}', 'ArticleController@update_article')->name('update_article');
//Route::get('get_single_articledata', 'ArticleController@get_single_articledata')->middleware('cors')->name('get_single_articledata');
Route::get('get_single_articledata', 'ArticleController@get_single_articledata')->name('get_single_articledata');


Route::get('orders', 'OrderController@index')->name('orders');
Route::get('get_orders', 'OrderController@get_orders')->name('get_orders');
Route::post('order_settings_saveform', 'OrderController@order_settings_saveform')->name('order_settings_saveform');
Route::get('edit_order/{id}', 'OrderController@edit_order')->name('edit_order');
Route::post('update_order/{id}', 'OrderController@update_order')->name('update_order');
Route::get('get_single_orderdata', 'OrderController@get_single_orderdata')->middleware('cors')->name('get_single_orderdata');


Route::get('customers', 'CustomerController@index')->name('customers');
Route::get('get_customers', 'CustomerController@get_customers')->name('get_customers');
Route::post('customer_settings_saveform', 'CustomerController@customer_settings_saveform')->name('customer_settings_saveform');
Route::get('edit_customer/{id}', 'CustomerController@edit_customer')->name('edit_customer');
Route::post('update_customer/{id}', 'CustomerController@update_customer')->name('update_customer');
Route::get('get_single_customerdata', 'CustomerController@get_single_customerdata')->middleware('cors')->name('get_single_customerdata');

Route::get('products', 'ProductController@index')->name('products');
Route::get('get_products', 'ProductController@get_products')->name('get_products');
Route::post('product_settings_saveform', 'ProductController@product_settings_saveform')->name('product_settings_saveform');
Route::get('edit_product/{id}', 'ProductController@edit_product')->name('edit_product');
Route::post('update_product/{id}', 'ProductController@update_product')->name('update_product');
Route::get('get_single_productdata', 'ProductController@get_single_productdata')->middleware('cors')->name('get_single_productdata');










